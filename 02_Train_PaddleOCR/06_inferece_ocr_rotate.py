from paddleocr import PaddleOCR, draw_ocr
import cv2
import numpy as np
import os
ocr = PaddleOCR(
        rec_model_dir=r"./inference/container_PP-OCRv3-v1.0_rec/", 
        rec_char_dict_path="/paddle/ppocr/utils/container_dict.txt",
        use_gpu=False,
        rec_image_shape="3, 48, 320",
        det=False,
        cls=False
            )
path_to_test_images = r"/paddle/test_images_container2"

ROOT_DIR = os.path.dirname(os.path.abspath(path_to_test_images)) # This is your Project Root

OUTPUT_LABEL_DIR = ROOT_DIR + "/labels"

if not os.path.exists(OUTPUT_LABEL_DIR):
    os.makedirs(OUTPUT_LABEL_DIR)


for image in os.listdir(path_to_test_images):
    opencv_image = cv2.imread(os.path.join(path_to_test_images, image))
    # rotated_img = cv2.rotate(opencv_image, cv2.ROTATE_90_COUNTERCLOCKWISE)
    rotated_img = opencv_image
    result       = ocr.ocr(rotated_img, cls=False, det=False)
    predicted    = result[0][0]
    label_name, conf = predicted
    print(os.path.join(path_to_test_images, image),predicted)
    
    
    basename = os.path.splitext(image)[0] + ".txt"
    with open(os.path.join(OUTPUT_LABEL_DIR, basename), "w") as file:
        file.write(label_name)