import cv2

label_file_path = "data/train_label.txt"  # Replace with the actual path to your label file

def load_labels_from_file(label_file_path):
    with open(label_file_path, "r") as file:
        lines = file.readlines()

    current_index = 0
    total_images = len(lines)

    while current_index < total_images:
        line = lines[current_index]
        image_path, label = line.strip().split("\t")
        image_path = "data/" + image_path
        print("Image Path:", image_path)
        print("Label:", label)

        # Load and display the image
        image = cv2.imread(image_path)
        cv2.imshow("Image", image)

        # Wait for a key press
        key = cv2.waitKey(0)

        # Check the pressed key
        if key == ord("d"):
            current_index += 1  # Move to the next image
        elif key == ord("a") and current_index > 0:
            current_index -= 1  # Move to the previous image
        elif key == ord("q"):
            break

    cv2.destroyAllWindows()

def enter_text():
    text = input("Enter text: ")
    print("Entered Text:", text)

# Example usage
load_labels_from_file(label_file_path)
enter_text()
