
import glob
import os 
import pandas as pd 

label_file = "/paddle/data/07022023_LPR_Lao/rec_gt.txt"
image_folder = "/paddle/data/07022023_LPR_Lao/crop_img"

img_paths = glob.glob(image_folder + "/*.jpg")

df = pd.read_csv(label_file, sep="\t", header=None)
df.columns = ['file_path', 'label']
label_paths = df['file_path'].values.tolist()
# print(label_paths)

cnt = 0
need_to_rows = []
print(df.shape)

data = []

for img_path in img_paths:
    basename = os.path.basename(img_path)
    basename = "crop_img/" + basename
    # label = os.path.splitext(basename)[0] + ".txt"
    if basename in label_paths:
        label_name = df[df['file_path'] == basename].label.values[0]
        data += [dict(file_path = basename, label=label_name)]
    else:
        cnt += 1
        print("Label Missed:", img_path)
        # remove the row in colummn
        # os.remove(img_path)

missed_img = 0
missed_img_idx = []
for idx, item in df.iterrows():
    img_path, label = item['file_path'], item['label']
    if not os.path.exists(os.path.join("/paddle/data/07022023_LPR_Lao/crop_img",os.path.basename(img_path))):
        missed_img += 1
        print(os.path.join("/paddle/data/07022023_LPR_Lao/crop_img",os.path.basename(img_path)))
        missed_img_idx.append(idx)
        # remove the image

df.drop(missed_img_idx, inplace=True)
df.to_csv("new_df_missed_idx.txt",index=False, header=False, sep="\t")

# new_df = pd.DataFrame(data)
# new_df.to_csv("output_train.txt", sep="\t", index=False, header=None)
print("Number missed label (have image)", cnt)
print("Number missed img  (have label)", missed_img)