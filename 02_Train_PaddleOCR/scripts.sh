# NOte: SƯ dụng use_amp: hàm loss không được hôi tụ
# Train Text Detection - MobilnetV3 - 10 hours 
# Best params: threshold 0.6 unclip_ratio = 1.0

1. Copy the label in `images` folder and paste into train_lpd_label.txt

# fist time 
python3 tools/train.py -c configs/det/det_mv3_db_lao_vn.yml -o Global.pretrained_model=./pretrain_models/MobileNetV3_large_x0_5_pretrained Optimizer.base_lr=0.001


# out of memory : python3 tools/train.py -c configs/det/det_res18_db_v2.0_lao_vn.yml -o  Global.pretrained_model=./pretrain_models/ResNet18_vd_pretrained 
python3 tools/infer_det.py -c ./configs/det/det_mv3_db_lao_vn.yml -o Global.infer_img=normal_case_lao Global.pretrained_model="./output/db_mv3/latest" Global.save_res_path=normal_case_lao/inferene PostProcess.box_thresh=0.65 PostProcess.unclip_ratio=1.2
python3 tools/infer_det.py -c ./configs/det/det_mv3_db_lao_vn.yml -o Global.infer_img=normal_case_lao Global.pretrained_model="./output/db_mv3/latest" Global.save_res_path=normal_case_lao/inferene 


python3 tools/train.py -c configs/det/det_mv3_db_lao_vn.yml -o Global.pretrained_model=./pretrain_models/MobileNetV3_large_x0_5_pretrained
python3 tools/infer_det.py -c configs/det/det_mv3_db_lao_vn.yml -o Global.infer_img=normal_case_lao Global.pretrained_model="./output/db_mv3/latest" Global.save_res_path=normal_case_lao/inferene PostProcess.unclip_ratio=1.2
python3 tools/export_model.py -c configs/det/det_mv3_db_lao_vn.yml -o Global.pretrained_model=./output/db_mv3/best_accuracy Global.save_inference_dir=./output/inference/det_mv3_db_lao_vn/

python3 tools/infer/predict_det.py --image_dir="./hard_case_lao" --det_model_dir="./output/inference/det_mv3_db_lao_vn"
python3 tools/infer/predict_det.py --image_dir="./normal_case_lao" --det_model_dir="./output/inference/det_mv3_db_lao_vn"

# train OCR                                                                                                                                                                                                                                                                             

# use the AMP=True => Error
python3 tools/train.py -c configs/rec/PP-OCRv3/vi_lao_PP-OCRv3_rec.yml -o Global.pretrained_model=pretrain_models/en_PP-OCRv3_rec_train/best_accuracy Global.save_model_dir=./output/v3_lao_mobile_072023_v2
# inference from checkpoint
python3 tools/infer_rec.py -c configs/rec/PP-OCRv3/vi_lao_PP-OCRv3_rec.yml -o Global.pretrained_model=output/v3_lao_mobile_072023_v2/best_accuracy  Global.infer_img=test_images Global.save_res_path="./test_images/predicts_rec.txt"
# export
python3 tools/export_model.py -c configs/rec/PP-OCRv3/vi_lao_PP-OCRv3_rec.yml -o Global.pretrained_model=output/v3_lao_mobile_072023_v2/best_accuracy  Global.save_inference_dir=./inference/v3_lao_mobile_070223_v2/
# inference from export
python3 tools/infer/predict_rec.py --image_dir="test_images/lao26.jpg" --rec_model_dir="./inference/lao_PP-OCRv3-v1.0_rec"  --rec_char_dict_path="ppocr/utils/vi_lao_dict.txt"


# train container
python3 tools/train.py -c configs/rec/PP-OCRv3/container_PP-OCRv3_rec.yml -o Global.pretrained_model=pretrain_models/en_PP-OCRv3_rec_train/best_accuracy Global.save_model_dir=./output/v3_container_mobile_v1.0_only_vertical_add_data
python3 tools/infer_rec.py -c configs/rec/PP-OCRv3/container_PP-OCRv3_rec.yml -o Global.pretrained_model=./output/v3_container_mobile_v1.0_only_vertical_add_data/best_accuracy  Global.infer_img=test_images_container
python3 tools/export_model.py -c configs/rec/PP-OCRv3/container_PP-OCRv3_rec.yml -o Global.pretrained_model=./output/v3_container_mobile_v1.0_only_vertical_add_data/best_accuracy  Global.save_inference_dir=./inference/container_PP-OCRv3-v1.0_rec/


# custom inference + detection
python3 tools/inference_det_rec.py


# Train detection model with AMP 
python tools/train.py -c ./configs/det/det_r50_vd_sast_icdar15_lao_vn.yml -o Global.use_amp=True
python paddle.distributed.launch --gpus '0,1' tools/train.py -c ./configs/det/ch_PP-OCRv3/ch_PP-OCRv3_det_student_lao_vn.yml -o Global.use_amp=True
python paddle.distributed.launch --gpus '0,1' tools/train.py -c ./configs/det/ch_PP-OCRv3/ch_PP-OCRv3_det_student_lao_vn.yml -o

python3 tools/export_model.py -c ./configs/det/ch_PP-OCRv3/ch_PP-OCRv3_det_student_lao_vn.yml -o Global.pretrained_model=output/ch_PP-OCR_V3_det/best_accuracy  Global.save_inference_dir=./inference/ch_PP-OCR_V3_det/


# Evaluation detection
python tools/eval.py -c ./configs/det/SAST.yml

# Predict detection
python3 tools/infer_det.py -c ./configs/det/SAST.yml -o Global.infer_img=

python3 tools/infer_det.py -c ./configs/det/ch_PP-OCRv3/ch_PP-OCRv3_det_student_lao_vn.yml -o Global.infer_img=test_images Global.use_gpu=False
# modify customm - use the checkpoint from training
python3 infer_det_custom.py -c ./configs/det/ch_PP-OCRv3/ch_PP-OCRv3_det_student_lao_vn.yml -o Global.infer_img=test_images Global.use_gpu=False


# cái này chỉ dùng cho model sau khi export 
python3 tools/infer/predict_det.py --image_dir="test_images" --det_model_dir="./output/ch_PP-OCR_V3_det" 
python3 tools/infer/predict_rec.py --image_dir="test_images" --det_model_dir="./inference/SAST" 


# Convert to inference Model
python3 tools/export_model.py -c ./configs/det/SAST.yml  

# Detection and recognition concatenate
python3 /content/drive/MyDrive/PaddleOCR/PaddleOCR/tools/infer/predict_system.py 
                --use_gpu=True \
                --det_algorithm="SAST" \
                --det_model_dir="./inference/SAST" \
                --rec_algorithm="SRN" \
                --rec_model_dir="./inference/SRN/" \
                --rec_image_shape="1, 64, 256" \
                --image_dir=#path_img \
                --rec_char_type="ch" \
                --drop_score=0.7 \
                --rec_char_dict_path="./ppocr/utils/dict/vi_vietnam.txt"


python3 tools/infer/predict_system.py --use_gpu=True --det_algorithm="DB" --det_model_dir="./inference/ch_PP-OCR_V3_det" --rec_algorithm="SVTR" --rec_model_dir="./inference/en_PP-OCRv3_rec" --rec_image_shape="3, 48, 320" --image_dir=test_images --rec_char_type="ch" --drop_score=0.7 --rec_char_dict_path="ppocr/utils/vi_lao_dict.txt"
python3 tools/infer/predict_system.py --use_gpu=True --det_algorithm="DB" --det_model_dir="./inference/ch_PP-OCR_V3_det" --rec_algorithm="SVTR" --rec_model_dir="./inference/en_PP-OCRv3_rec" --rec_image_shape="3, 48, 320" --image_dir=test_images --drop_score=0.7 --rec_char_dict_path="ppocr/utils/vi_lao_dict.txt"


#### Update

python3 tools/infer_det.py -c ./configs/det/ch_PP-OCRv3/ch_PP-OCRv3_det_student_lao_vn.yml -o Global.pretrained_model="./output/ch_PP-OCR_V3_det/latest"  Global.infer_img=test_images

python3 tools/export_model.py -c ./configs/det/ch_PP-OCRv3/ch_PP-OCRv3_det_student_lao_vn.yml -o Global.infer_shape="3,23Global.pretrained_model=output/ch_PP-OCR_V3_det/latest  Global.save_inference_dir=./inference/ch_PP-OCR_V3_det/


python3 tools/export_model.py -c ./configs/det/ch_PP-OCRv3/ch_PP-OCRv3_det_student_lao_vn.yml -o Global.save_inference_dir=./inference/ch_PP-OCR_V3_det/

python3 tools/infer/predict_system.py --use_gpu=True --det_algorithm="DB" --det_model_dir="./inference/ch_PP-OCR_V3_det" \
--rec_algorithm="SVTR" \
--rec_model_dir="./inference/en_PP-OCRv3_rec" \
--rec_image_shape="3, 48, 320" --image_dir=test_images --drop_score=0.7 --rec_char_dict_path="ppocr/utils/vi_lao_dict.txt"


python3 tools/infer/predict_det.py --det_model_dir="./inference/ch_PP-OCR_V3_det/" --image_dir=test_images
python3 tools/infer/predict_det.py --det_model_dir="./ch_PP-OCRv3_det_infer/" --image_dir=test_images