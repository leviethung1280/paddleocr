import os
import uvicorn
from paddleocr import PaddleOCR, draw_ocr
from PIL import Image, ImageDraw
import cv2
import io 
import numpy as np 
import glob

THRESHOLD_CONF = 0.6
DICT_PATH = 'ppocr/utils/vi_lao_dict.txt'
DEBUG = True

# en_ocr = PaddleOCR(lang="ch", det_model_dir="./inference/Multilingual_PP-OCRv3_det_infer", rec_model_dir="./inference/en_PP-OCRv3_rec")

# use custom model
# en_ocr = PaddleOCR(det_model_dir="./inference/Multilingual_PP-OCRv3_det_infer", rec_model_dir="./inference/en_PP-OCRv3_rec", rec_char_dict_path="ppocr/utils/vi_lao_dict.txt")

# test with new detection on 4-6-2023
en_ocr = PaddleOCR(rec_image_shape = "", det_algorithm="DB", det_model_dir="./inference/ch_PP-OCR_V3_det", rec_model_dir="./inference/en_PP-OCRv3_rec", rec_char_dict_path="ppocr/utils/vi_lao_dict.txt")

def load_dictionary():
    with open(DICT_PATH, "r") as file:
        content = file.readlines()
        content = [item.strip() for item in content]
    return content

char_dict = load_dictionary()

def filter_special_character(char_list, input_string):
    final_string = []
    for char in input_string:
        if char in char_dict:
            final_string.append(char)
    return final_string

def parse_result(results, threshold, image_pil, output_dir, filename):
    if DEBUG:
        boxes = [line[0] for line in results[0]]
        txts = [line[1][0] for line in results[0]]
        scores = [line[1][1] for line in results[0]]

        print(boxes)
        print(txts)
        print(scores)

        im_show = draw_ocr(image_pil, boxes, txts, scores, drop_score = 0.5, font_path="fonts/simfang.ttf")
        print(os.path.join(output_dir, filename))
        cv2.imwrite(os.path.join(output_dir, filename), im_show)

    result = results[0]
    regions = [line[0] for line in result]

    texts, scores = [], []
    for line in result:
        text, score = line[1][0], line[1][1]
        if score > threshold:
            texts.append(text)
            scores.append(score)
    
    # print("regions", regions)
    # print("texts", texts)
    # print("scores", scores)
    
    recognized_text = ""
    for text in texts:
        recognized_text += text
    
    print("Before Filtering: ", recognized_text)
    recognized_text = filter_special_character(char_dict, recognized_text)
    recognized_text = "".join(recognized_text)
    print("Afte Filtering: ", recognized_text)
        
    dict_data = {
        "regions": regions,
        "texts" : texts,
        "scores": scores
    }

    return recognized_text, dict_data

def save_image(image_array, save_path):
    # Create a PIL Image from the image array
    image_pil = Image.fromarray(image_array)

    # Save the image to the specified path
    image_pil.save(save_path)


def draw_bounding_boxes(image_array, regions, color=(0, 255, 0), thickness=1):
    # Create a PIL Image from the image array
    image_pil = Image.fromarray(np.uint8(image_array))

    # Create a PIL ImageDraw object
    draw = ImageDraw.Draw(image_pil)
    
    # Iterate over each regions
    for region in regions:
        tl, tr, br, bl = region
        x_min, y_min = tl
        x_max, y_max = br
        x_min, y_min, x_max, y_max = int(x_min), int(y_min), int(x_max), int(y_max)
        
        draw.rectangle([x_min, y_min, x_max, y_max], outline=color, width=thickness)

    # Convert the PIL Image back to a NumPy array
    image_with_boxes = np.array(image_pil)

    return image_with_boxes

def parse_base64_to_numpy(base64_string):
    # Remove the data URL prefix if present
    if base64_string.startswith('data:image'):
        base64_string = base64_string.split(',')[1]

    # Decode the Base64 string
    image_data = base64.b64decode(base64_string)

    # Create a BytesIO object
    image_bytes = io.BytesIO(image_data)

    # Open the image using PIL
    pil_image = Image.open(image_bytes)
     
    if pil_image.mode != "RGB":
        pil_image = pil_image.convert("RGB")

    # Convert PIL image to NumPy array
    numpy_array = np.array(pil_image)

    return numpy_array

if __name__ == "__main__":
    # uvicorn.run(app, host="0.0.0.0", port=8000)
    INPUT_FOLDER = "test_images"
    images= glob.glob(INPUT_FOLDER + "/*.jpg")
    output_dir = "output/images"
    for image_path in images:
        image_path = os.path.join( image_path)
        image_pil = Image.open(image_path)
        results = en_ocr.ocr(image_path, det=True, rec = True, cls=False)
        print("resuts:",results)
        recognized_text, dict_data = parse_result(results,0.6,image_pil, output_dir=output_dir, filename= os.path.basename(image_path))
        print(recognized_text)
        print(f"Check the output_dir: {output_dir}")
