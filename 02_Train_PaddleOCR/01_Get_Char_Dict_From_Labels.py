import os 
def parse_label_file(label_file_path):
    character_set = set()

    with open(label_file_path, "r") as file:
        lines = file.readlines()
        
    print(lines, label_file_path)

    for line in lines:
        print(line)
        _, label = line.strip().split("\t")  # Assuming the format <x1>,<y1>,<x2>,<y2>,<label>
        character_set.update(label)

    return character_set


def generate_character_dict(label_file_paths):
    character_set = set()

    for label_file_path in label_file_paths:
        character_set.update(parse_label_file(label_file_path))

    character_dict = {char: index for index, char in enumerate(character_set)}
    character_dict_filtered = {char: index for char, index in character_dict.items() if char != ' '}

    return character_dict_filtered


def save_character_dict(character_dict, output_file_path):
    # sort the character_idct
    sorted_dict = {k: character_dict[k] for k in sorted(character_dict)}
    
    print("Number of character: ", len(sorted_dict))
    with open(output_file_path, "w") as file:
        last_item = list(sorted_dict)[-1]
        for character in sorted_dict:
            if character != last_item:
                file.write(character + "\n")
            else:
                file.write(character)
                


label_file_paths = ["data/02_20_July2023_LPR_Lao/rec_gt.txt"]
root_folder = os.path.dirname(os.path.abspath(label_file_paths[0]))
character_dict = generate_character_dict(label_file_paths)
output_file_path = f"{root_folder}/character_dict.txt"
save_character_dict(character_dict, output_file_path)
