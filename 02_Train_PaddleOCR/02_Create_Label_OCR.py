# # %%
# import os 

# directory = "/media/hunglv/350GB_1TB_HDD/Lao_LPR/Results/images"
# labels_list = []


# for filename in os.listdir(directory):
#     if filename.endswith(".jpg") or filename.endswith(".png"): 
#         # Make sure only image files are included
#         filepath = os.path.join(directory, filename)
#         print(filepath)
#         print(filename)
#         label = filename.split('.')[0]
#         labels_list.append(f"{filepath}\t{label}")


# print(labels_list)

# with open("labels.txt", "w") as file:
#     file.write('\n'.join(labels_list))
    
# # %% 
import os 
import glob



FOLDER_PATH = "/paddle/data/images"

labels_list = []
labels = glob.glob(FOLDER_PATH + "/*.txt")
for label in labels:
    # read content
    with open(label, "r") as file:
        content = file.read()
        print(content)
    basename = os.path.basename(label).split(".")[0]
    labels_list.append(f"{basename}\t{content}")
    

with open(f"{FOLDER_PATH}/labels", "w") as file:
    file.write('\n'.join(labels_list))