# Remove the space and ".", "-" in the label
label_file_path = "/paddle/data/Lao_Thanh_Annotation/labels.txt"  # Replace with the actual path to your label file

character_set = set()

# Read the label file
with open(label_file_path, "r") as file:
    lines = file.readlines()

labels_list = []

# Parse the label file and generate the character dictionary
for line in lines:
    image_path, label = line.strip().split("\t")  # Assuming the format <image_path>\t<label>
    label = label.replace(" ", "").replace("-", "").replace(".", "")  # Remove spaces, "-", and "."
    character_set.update(label)
    labels_list.append(f"{image_path}\t{label}")

with open("new.txt", "w") as file:
    file.write("\n".join(labels_list))