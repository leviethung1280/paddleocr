import cv2
import matplotlib.pyplot as plt
import glob 
import matplotlib
import os 
matplotlib.use('TkAgg')
import time

def plot_images_and_labels(root_folder, label_file, save_file=None, query_label=None):
    # Load the labels from the text file
    with open(label_file, 'r') as f:
        labels = f.readlines()

    # Remove any trailing newline characters
    labels = [label.strip() for label in labels]
    
    # fitler the lable by query label 
    filtered_labels = []
    for label in labels:
        if query_label and query_label in label:
            filtered_labels.append(label)

    if query_label is None:
        filtered_labels = labels
        
    # filtered_labels = filtered_labels[0:50]
            
    # Create a figure to display the images
    fig = plt.figure(figsize=(20,10))
    columns = 7
    # rows = (len(filtered_labels) + columns - 1) // columns
    
    max_image_per_page = 50
    rows = (max_image_per_page + columns - 1) // columns
    
    print(rows)

    cnt = 0
    # Iterate over the image paths and labels
    for i, label in enumerate(filtered_labels):
        # Read the image using OpenCV
        
        
        image_path = label.split("\t")[0]
        label_name = label.split("\t")[1]
        
        image = cv2.imread(os.path.join(root_folder,image_path))
        print(os.path.join(root_folder,image_path))
        print(image.shape)
        
        # Convert BGR to RGB color format
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # Add a subplot for the current image
        ax = fig.add_subplot(rows, columns, cnt+1)
        
        # Display the image
        ax.imshow(image)
        ax.axis('off')
        

        # Show the label below the image
        ax.text(0.5, -0.5, label_name, fontsize=15, ha='center', transform=ax.transAxes, color='red')
    
        cnt += 1
        if cnt > 50:
            # Adjust the layout and display the figure
            plt.tight_layout()
                
            if save_file:
                plt.savefig(f"{time.time()}_{save_file}")
                print("Figure saved as", save_file)
            # plt.show()
            fig = plt.figure(figsize=(20,10))
            cnt = 0

# plot_images_and_labels("data/07012023_LPR_ChienKhuong_v1_2", "/paddle/data/07012023_LPR_ChienKhuong_v1_2/rec_gt.txt",save_file='gallery.png', query_label="ຣ")
# plot_images_and_labels("/paddle", "/paddle/test_images/predicts_rec.txt",save_file='gallery.png', query_label="ຣ")

# ! Want to use the label , root_dir = ""
plot_images_and_labels("", "/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output/labels.txt",  save_file="gallery.png")