import cv2
import os 
import numpy as np
from PIL import Image, ImageDraw, ImageFont
import pandas as pd 

label_file_path = "/paddle/data/07012023_LPR_ChienKhuong_v1_2/rec_gt.txt"  # Replace with the actual path to your label file
FONT_PATH = 'fonts/lao_saysettha_ot.ttf'

class PILImage:
    def __init__(self, font_path, font_size=30, font_color=(255,0,0)):
        self.font_color = (255,0,0)
        self.font_size = 25
        self.font_path = font_path
        self.width = None
        self.height = None
    
    def create_white_image(self, width, height):
        self.width = width
        self.height = height
        white_image = Image.new("RGB", (width, height), "white")
        return white_image
    
    def write_text(self,image,text):
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype(self.font_path,self.font_size)
        
        text_width, text_height  = font.getsize(text)
        
        # Determine the position to place the text
        position = (5,5)
        draw.text(position, text, font=font, fill=self.font_color)        

    def convert_opencv(self, pil_image):
        np_array  = np.array(pil_image)
        return cv2.cvtColor(np_array, cv2.COLOR_RGB2BGR)


def write_text(image, text):
    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 1.5

    # Choose the color and thickness of the text
    color = (0, 255, 0)  # Green color
    thickness = 2

    # Calculate the width and height of the text box
    text_size, _ = cv2.getTextSize(text, font, font_scale, thickness)
    text_width, text_height = text_size

    # Determine the position to place the text
    x = int((image.shape[1] - text_width) / 2)
    y = int((image.shape[0] + text_height) / 2)

    # Draw the text on the image
    cv2.putText(image, text, (x, y), font, font_scale, color, thickness)

def load_labels_from_file(root_dir, label_file_path, query_label=None):
    with open(label_file_path, "r") as file:
        lines = file.readlines()
        lines = [line.strip() for line in lines]
        
    filtered_lines = []
    for line in lines:
        label = line.strip().split("\t")[1]
        if query_label in label:
            filtered_lines.append(line)
    
        
    df = pd.read_csv(label_file_path, sep="\t", header=None)
    df.columns = ['file_path', 'label']

    current_index = 0
    total_images = len(filtered_lines)
    
    updated_labels = []

    while current_index < total_images:
        line = filtered_lines[current_index]
        image_path, label = line.strip().split("\t")
        image_path = os.path.join(root_dir, image_path)
        print("Image Path:", image_path)
        print("Label:", label)
        
        
        condition = df['file_path'] == line.strip().split("\t")[0]
        
        # Load and display the image
        image = cv2.imread(image_path)
        height, width, _ = image.shape
                
        pil_cls = PILImage(font_path=FONT_PATH)
        white_image = pil_cls.create_white_image(width, height)
        pil_cls.write_text(white_image, label)

        # Choose the font type and scale
        horizontal_concat = np.concatenate((image, white_image), axis=1)
        
        # stack to current image
        cv2.imshow("Image", horizontal_concat)

        # Wait for a key press
        key = cv2.waitKey(0)

        # Check the pressed key
        if key == ord("d"):
            current_index += 1  # Move to the next image
        elif key == ord("a") and current_index > 0:
            current_index -= 1  # Move to the previous image
        elif key == ord("q"):
            break
        elif key == ord("e"):
            updated_label = enter_text()
            df.loc[condition, 'label'] = updated_label
        elif key == ord("w"):
            # export the label 
            df.to_csv(label_file_path, sep='\t', header=False, index=False)                
            
            
    cv2.destroyAllWindows()

def enter_text():
    text = input("Enter text: ")
    print("Entered Text:", text)
    return text
# Example usage
load_labels_from_file(root_dir="data/07012023_LPR_ChienKhuong_v1_2",
                      label_file_path=label_file_path,
                      query_label="ຣ")
