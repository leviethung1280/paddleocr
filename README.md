## Document
https://docs.google.com/spreadsheets/d/1JUG1eSquhILnUQn-X9fYzt2fiQ0Fl6JB_9eU5-xW_Cg/edit?usp=sharing

## Build the PaddleOCR 

```bash
docker-compose up -d --build
```

## LP Classification
4 labels:
- Vietnam_1_Line
- Vietnam_2_Line
- China
- Cambodia
- Lao

## How to use

Send the POST request to API: http://localhost:8000/parse

vn_cam
```
{
  "base64": "string",
  "threshold": 0.6,
  "language": "vn_cam"
}
```

vn_ch
```
{
  "base64": "string",
  "threshold": 0.6,
  "language": "vn_ch"
}
```

Options:
- language: "vn_cam", "vn_ch`"
- threshold: confidence score filtered before send to client
- base64 string: "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyM"

## Base64 Image Encoding
Encode the image to base 64: https://base64.guru/converter/encode/image


## Development

Add the path of project to the PYTHONPATH
```
export PYTHONPATH="${PYTHONPATH}:/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr"
python src/main.py 
```

## How to run the test case

```bash
python -m pytest test/test_api.py -v
```

Test specific function 
```bash
python -m pytest test/test_api.py -k test_parse_lao100 -o log_cli=True
python -m pytest test/test_api.py -k test_parse_lao_17072023 -o log_cli=True
```


OCR model input: (img, (np.ndarray, list, str, bytes))


## Version Log 
v1.0: Use default model of PaddleOCR
v2.0: Change the model to PaddleOCRSharp
v2.1: Remove the lowercase letter in vn_dict.txt
v2.2: 
- Model PaddleOCRShrap -> vn_cam: change the name of variable
- Add Model Chinese for inference
- Add test cases 
- Add rotating function
v2.3:
- Add LPR Classification model
- Add new logic with LPR clasification modelgit 
v2.4:
- Add the lowercase for Vietnamese character and then uppercase 
- Remove the rule base for 0 letter -> D
- Add model Lao
v2.5:
- Update the rule base for Lao
- Add test case for Lao 
- Fix text detection by filtering the maximum area of boxes
- Turn on the text detection for Lao Plates
v2.5.1:
- Remove the Rule-based for Lao 
- Add re-recognition for Lao plates
- Update the test_api to save the result

v2.5.2 & v2.5.3
- Fix bug chinese site local variable reference

v2.5.4
- Add the logging feature
- Fix bug Lao Model
- Update the classification for each country

v2.5.5
- Update the algorithm for selectin the regions
- Update model Lao

v2.5.5.1
- Fix Classification Algorithm

v2.5.5.2
- 17/07: Update Text Detection at Lao country 
- Refactor train code PaddleOCR

v2.5.5.3
- 22/07: Update Rec MOdel in Lao country - v2  (added 1000 images)
- 23/07: 
  - Update Detection Model in Lao country - v2 (added 1000 images)
  - Re-write the inference for text detection, get the original detection boxes -> better recognition results
atus