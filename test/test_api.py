import base64
import os
import unittest

import pandas as pd
import requests
from loguru import logger

from src.utils import get_images, open_base64, insert_images_auto_scale

HOST = "0.0.0.0"
PORT = "8500"


class TestImageUpload(unittest.TestCase):
    def test_upload_image(self):
        # get all image paths in test directory
        test_dir = "./test_images"
        image_paths = [os.path.join(test_dir, f) for f in os.listdir(test_dir) if
                       os.path.isfile(os.path.join(test_dir, f))]

        assert len(image_paths) > 0
        results = []
        for image_path in image_paths:
            # convert image to base64
            with open(image_path, "rb") as f:
                image_data = f.read()
                encoded_data = base64.b64encode(image_data).decode("utf-8")

            # prepare request data
            lang = "vn_cam"
            data = {"language": lang, "base64": encoded_data}

            response = requests.post(f"http://{HOST}:{PORT}/parse", json=data)

            # check status code is 200
            assert response.status_code == 200

            # check the returned filename matches the original image filename
            result = response.json()["text"]
            filename = os.path.basename(image_path).split('.')[0]

            print("Prediction: ", result)

            results.append(result)

        basenames = [os.path.basename(image_path).split('.')[0] for image_path in image_paths]
        assert results == basenames

    def read_label(self, path, filenames):
        # preprocessing 
        filenames = [os.path.basename(filename) for filename in filenames]

        df = pd.read_csv(path, header=None, sep="\t")
        df.columns = ["img_path", "label"]

        labels = []

        for filename in filenames:
            label = df[df.img_path == filename].label.values[0]
            labels.append(label)

        return labels

    def check_accuracy(self, list1, list2):

        matches = 0
        for i in range(len(list1)):
            if list1[i] == list2[i]:
                matches += 1

        accuracy = matches / len(list1)

        return accuracy

    def test_parse_lao_image(self, lang="vn_lao"):
        # get all image paths in test directory
        test_dir = "./test/lao_images"
        label_path = test_dir + "/rec_gt.txt"

        image_paths = get_images(test_dir)
        assert len(image_paths) > 0

        results = []

        for image_path in image_paths:
            # convert image to base64
            with open(image_path, "rb") as f:
                image_data = f.read()
                encoded_data = base64.b64encode(image_data).decode("utf-8")

            # prepare request data
            data = {"language": lang, "base64": encoded_data}

            response = requests.post(f"http://{HOST}:{PORT}/parse", json=data)

            # check status code is 200
            assert response.status_code == 200

            # check the returned filename matches the original image filename
            result = response.json()["text"]
            filename = os.path.basename(image_path).split('.')[0]

            print("Prediction: ", result)

            results.append(result)

        gt = self.read_label(label_path, image_paths)

        acc = self.check_accuracy(gt, results)
        print("Accuracy:", acc)
        self.assertEqual(acc, 100)

    def test_local_lao_image(self, lang="vn_lao"):
        # get all image paths in test directory
        test_dir = "/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_images"
        image_paths = get_images(test_dir)

        assert len(image_paths) > 0

        # convert image to base64
        data = {"language": lang, "folder_dir": test_dir}

        response = requests.post(f"http://{HOST}:{PORT}/test_local", params=data)

        # check status code is 200
        assert response.status_code == 200

        # check the returned filename matches the original image filename
        result = response.json()["text"]

    def test_parse_lao_image(self, lang="vn_lao"):
        # get all image paths in test directory
        test_dir = "/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_images"
        image_paths = get_images(test_dir)

        assert len(image_paths) > 0
        results = []
        for image_path in image_paths:
            # convert image to base64
            with open(image_path, "rb") as f:
                image_data = f.read()
                encoded_data = base64.b64encode(image_data).decode("utf-8")

            # prepare request data

            data = {"language": lang, "base64": encoded_data}

            response = requests.post(f"http://{HOST}:{PORT}/parse", json=data)

            # check status code is 200
            assert response.status_code == 200

            # check the returned filename matches the original image filename
            result = response.json()["text"]
            filename = os.path.basename(image_path).split('.')[0]

            print("Prediction: ", result)

            results.append(result)

        basenames = [os.path.basename(image_path).split('.')[0] for image_path in image_paths]
        assert results == basenames

    def test_parse_lao_image_chienkhuong(self, lang="vn_lao"):
        os.system("rm -rf /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")
        os.system("mkdir /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")

        # get all image paths in test directory
        test_dir = "/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_hard"
        image_paths = get_images(test_dir)

        assert len(image_paths) > 0
        results = []

        file = open("/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output/labels.txt", "w")

        for image_path in image_paths:
            # convert image to base64
            with open(image_path, "rb") as f:
                image_data = f.read()
                encoded_data = base64.b64encode(image_data).decode("utf-8")

            # prepare request data

            data = {"language": lang, "base64": encoded_data}

            response = requests.post(f"http://{HOST}:{PORT}/parse", json=data)

            # check status code is 200
            assert response.status_code == 200

            # check the returned filename matches the original image filename
            result = response.json()["text"]
            filename = os.path.basename(image_path).split('.')[0]

            print("Prediction: ", result)
            # write the labels
            results.append(result)

            file.write(f"{image_path}\t{result}\n")

        basenames = [os.path.basename(image_path).split('.')[0] for image_path in image_paths]

        file.close()

        assert results == basenames

    def test_parse_inference(self, lang="vn_cam"):
        os.system("rm -rf /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")
        os.system("mkdir /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")

        # get all image paths in test directory
        test_dir = "/mnt/01D624E6112E18C0/01_Projects/14_Container_Repos/ContainerNumber-OCR_5_Stars/container_number_recognition/ContainerNum_dataset/01_LPR/images"

        image_paths = get_images(test_dir)

        assert len(image_paths) > 0
        results = []

        file = open("/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output/labels.txt", "w")

        for image_path in image_paths:
            # convert image to base64
            with open(image_path, "rb") as f:
                image_data = f.read()
                encoded_data = base64.b64encode(image_data).decode("utf-8")

            # prepare request data

            data = {"language": lang, "base64": encoded_data}

            response = requests.post(f"http://{HOST}:{PORT}/parse", json=data)

            # check status code is 200
            assert response.status_code == 200

            # check the returned filename matches the original image filename
            result = response.json()["text"]
            filename = os.path.basename(image_path).split('.')[0]

            print("Prediction: ", result)
            # write the labels
            results.append(result)

            file.write(f"{image_path}\t{result}\n")

        basenames = [os.path.basename(image_path).split('.')[0] for image_path in image_paths]

        file.close()

        assert results == basenames

    def test_parse_lao225(self, lang="vn_lao", save_prediction=True):
        os.system("rm -rf /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")
        os.system("mkdir /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")

        corrected_predictions = 0

        # get all image paths in test directory
        test_dir = "/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao225"

        image_paths = get_images(test_dir)

        assert len(image_paths) > 0

        results = []

        file = open("/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output/labels.txt", "w")

        for image_path in image_paths:
            # convert image to base64
            with open(image_path, "rb") as f:
                image_data = f.read()
                encoded_data = base64.b64encode(image_data).decode("utf-8")

            # prepare request data

            data = {"language": lang, "base64": encoded_data}

            response = requests.post(f"http://{HOST}:{PORT}/parse", json=data)

            # check status code is 200
            assert response.status_code == 200

            # check the returned filename matches the original image filename
            result = response.json()["text"]
            filename = os.path.basename(image_path).split('.')[0]

            print("Prediction: ", result)
            # write the labels
            results.append(result)

            file.write(f"{image_path}\t{result}\n")

        file.close()

        total_predictions = len(image_paths)
        accuracy = corrected_predictions * 100 / total_predictions
        logger.critical(f"Corrected predictions: {corrected_predictions}")
        logger.critical(f"Total predictions: {total_predictions}")
        logger.critical(f"Acc: {accuracy}")
        assert accuracy > 95, "Accuracy should be greater than 95%"

    def test_parse_cam700(self, lang="vn_cam"):
        os.system("rm -rf /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")
        os.system("mkdir /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")

        corrected_predictions = 0

        # get all image paths in test directory
        test_dir = "/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/cam700"

        image_paths = get_images(test_dir)

        assert len(image_paths) > 0

        results = []

        file = open("/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output/labels.txt", "w")

        last_item = image_paths[-1]
        for image_path in image_paths:
            # convert image to base64
            with open(image_path, "rb") as f:
                image_data = f.read()
                encoded_data = base64.b64encode(image_data).decode("utf-8")

            # prepare request data

            data = {"language": lang, "base64": encoded_data}

            response = requests.post(f"http://{HOST}:{PORT}/parse", json=data)

            # check status code is 200
            assert response.status_code == 200

            # check the returned filename matches the original image filename
            result = response.json()["text"]
            language = response.json()['country']

            print(f"Prediction: {result} | {language}")
            # write the labels
            results.append(result)

            if image_path == last_item:
                file.write(f"{image_path}\t{result}")
            else:
                file.write(f"{image_path}\t{result}\n")

        file.close()

        total_predictions = len(image_paths)
        accuracy = corrected_predictions * 100 / total_predictions
        logger.critical(f"Corrected predictions: {corrected_predictions}")
        logger.critical(f"Total predictions: {total_predictions}")
        logger.critical(f"Acc: {accuracy}")
        assert accuracy > 95, "Accuracy should be greater than 95%"

    def test_parse_chinese1400(self, lang="vn_ch"):
        os.system("rm -rf /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")
        os.system("mkdir /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")

        corrected_predictions = 0

        # get all image paths in test directory
        test_dir = "/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/chinese1400"
        image_paths = get_images(test_dir)

        assert len(image_paths) > 0

        results = []

        file = open("/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output/labels.txt", "w")

        last_item = image_paths[-1]
        for image_path in image_paths:
            # convert image to base64
            with open(image_path, "rb") as f:
                image_data = f.read()
                encoded_data = base64.b64encode(image_data).decode("utf-8")

            # prepare request data

            data = {"language": lang, "base64": encoded_data}

            response = requests.post(f"http://{HOST}:{PORT}/parse", json=data)

            # check status code is 200
            assert response.status_code == 200

            # check the returned filename matches the original image filename
            result = response.json()["text"]
            language = response.json()['country']
            filename = os.path.basename(image_path).split('.')[0]

            print(f"Prediction: {result} | {language}")
            # write the labels
            results.append(result)

            if image_path == last_item:
                file.write(f"{image_path}\t{result}")
            else:
                file.write(f"{image_path}\t{result}\n")

        file.close()

        total_predictions = len(image_paths)
        accuracy = corrected_predictions * 100 / total_predictions
        logger.critical(f"Corrected predictions: {corrected_predictions}")
        logger.critical(f"Total predictions: {total_predictions}")
        logger.critical(f"Acc: {accuracy}")
        assert accuracy > 95, "Accuracy should be greater than 95%"

    def test_parse_lao_17072023(self, lang="vn_lao"):
        os.system("rm -rf /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")
        os.system("mkdir /mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output")

        corrected_predictions = 0

        test_dir = "/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/01_Preprocess_Data_Lao_Filter1"

        image_paths = get_images(test_dir)

        assert len(image_paths) > 0

        predictions = []

        file = open("/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/output/labels.txt", "w")

        last_item = image_paths[-1]
        image_paths = image_paths[0:10]
        
        for image_path in image_paths:
            # prepare request data
            encoded_data = open_base64(image_path)

            data = {"language": lang, "base64": encoded_data}

            response = requests.post(f"http://{HOST}:{PORT}/parse", json=data)

            # check status code is 200
            assert response.status_code == 200

            # check the returned filename matches the original image filename
            prediction = response.json()["text"]
            language = response.json()['country']
            # this label may be right or wrong, just for reference
            relative_label = os.path.basename(image_path).split('_')[0]

            print(f"Prediction: {prediction} | {language}")
            # write the labels
            predictions.append(prediction)

            if relative_label != prediction:
                print(f"Check again: {image_path}")
                print(f"Old Model: {relative_label} => New Label: {prediction}")

            if image_path == last_item:
                file.write(f"{image_path}\t{prediction}")
            else:
                file.write(f"{image_path}\t{prediction}\n")

        file.close()

        insert_images_auto_scale(image_paths, predictions, 'output.xlsx', start_row=2, max_width=130, max_height=130,
                                 cell_image_width=25)

        total_predictions = len(image_paths)
        accuracy = corrected_predictions * 100 / total_predictions
        logger.critical(f"Corrected predictions: {corrected_predictions}")
        logger.critical(f"Total predictions: {total_predictions}")
        logger.critical(f"Acc: {accuracy}")
        assert accuracy > 95, "Accuracy should be greater than 95%"
