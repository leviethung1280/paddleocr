import logging

class ColoredFormatter(logging.Formatter):
    COLOR_CODES = {
        logging.DEBUG: "\033[1;32m",  # Green
        logging.INFO: "\033[1;34m",   # Blue
        logging.WARNING: "\033[1;33m",# Yellow
        logging.ERROR: "\033[1;31m",  # Red
        logging.CRITICAL: "\033[1;41m" # White on Red background
    }
    
    def format(self, record):
        levelname = record.levelname
        if levelname in ["WARNING", "ERROR", "CRITICAL"]:
            record.msg = self.COLOR_CODES.get(record.levelno) + str(record.msg) + "\033[0m"
        return super().format(record)