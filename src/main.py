import base64
import copy
import importlib
import io
import logging.config
import os
import sys
import time
import unicodedata
import uuid
from typing import Optional

import cv2
import dotenv
import numpy as np
import uvicorn
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from fastapi import FastAPI, UploadFile, File, Query
from pydantic import BaseModel

dotenv.load_dotenv()

__dir__ = os.path.dirname(__file__)
sys.path.append(os.path.join(__dir__, ''))
paddleocr = importlib.import_module(".", "paddleocr")

from paddleocr import PaddleOCR, draw_ocr
from color_formatter import ColoredFormatter
from yolo_classification import YOLOClassification
from src.paddleocr.tools.infer.predict_system import sorted_boxes
from src.paddleocr.tools.infer.utility import get_rotate_crop_image

app = FastAPI()

THRESHOLD_CONF = float(os.getenv("THRESHOLD_CONF"))
DEBUG = bool(os.getenv("DEBUG"))
LAO_RATIO_REGION = float(os.getenv("LAO_RATIO_REGION"))

# Initialize logging
logfile = open('src/app.log', 'a', encoding='utf-8')
logging.basicConfig(stream=logfile,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s')
logger = logging.getLogger(__name__)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
formatter = ColoredFormatter('[%(levelname)s] %(message)s')
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)

CHAR_DICT_PATH = {
    "vn_cam": "src/char_dict/vn_cam.txt",
    "vn_lao": f"{os.getenv('LAO_REC_MODEL_DIR')}/vi_lao_dict.txt",
    "vn_ch": "src/char_dict/ch_en_dict.txt"
}

# vn_cam: PaddleOCR Sharp
vn_cam = PaddleOCR(lang="ch", det_model_dir="./src/models/ocr/ocr_sharp/en_v3/en_PP-OCRv3_det_infer",
                   det_db_box_thresh=0.618,
                   det_db_score_mode='slow', det_db_unclip_ratio=1.6,
                   rec_model_dir="./src/models/ocr/ocr_original/ocr_rec/ch_PP-OCRv3_rec_infer",
                   cls_model_dir=os.getenv("CLS_MODEL_DIR"))

logger.info("Loaded VN Cam Model")

# vn_ch: PaddleOCR Sharp: ch_PP-OCRv3
vn_ch = PaddleOCR(lang="ch", det_model_dir="./src/models/ocr/ocr_sharp/ch_v3/ch_PP-OCRv3_det_infer",
                  det_db_box_thresh=0.618,
                  det_db_score_mode='slow', det_db_unclip_ratio=1.6,
                  rec_model_dir="./src/models/ocr/ocr_original/ocr_rec/ch_PP-OCRv3_rec_infer",
                  cls_model_dir=os.getenv("CLS_MODEL_DIR"))
logger.info("Loaded VN China Model")

vn_lao = PaddleOCR(lang='ch', det_model_dir=f"{os.getenv('LAO_DET_MODEL_DIR')}", det_db_box_thresh=0.6,
                   det_db_score_mode='slow',
                   det_db_unclip_ratio=1.6,
                   rec_model_dir=f"{os.getenv('LAO_REC_MODEL_DIR')}",
                   rec_char_dict_path=f"{os.getenv('LAO_REC_MODEL_DIR')}/vi_lao_dict.txt",
                   cls_model_dir=os.getenv("CLS_MODEL_DIR"))

logger.info("Loaded VN Lao Model")

lp_cls_model = YOLOClassification(os.getenv("LP_CLS_PATH"))

# run the first inference for lp_cls_model
lp_cls_model.initialize()

logger.info("[*] Loaded License Plate Classification Model")

if not os.path.exists("src/output"):
    os.makedirs("src/output")


def load_dictionary(dict_path):
    with open(dict_path, "r") as file:
        content = file.readlines()
        content = [item.strip() for item in content]
    return content


vn_cam_char_dict = load_dictionary(CHAR_DICT_PATH['vn_cam'])
vn_ch_char_dict = load_dictionary(CHAR_DICT_PATH['vn_ch'])
vn_lao_char_dict = load_dictionary(CHAR_DICT_PATH['vn_lao'])

country_dict = {
    "Vietnam_1_Line": "Vietnam",
    "Vietnam_2_Line": "Vietnam",
    "Cambodia": "Cambodia",
    "China": "China",
    "Lao": "Lao"
}


class MyRequest(BaseModel):
    base64: str
    threshold: float = THRESHOLD_CONF
    language: str = "vn_cam"


def crop_bottom_half(img):
    height, width = img.shape[:2]
    cropped_img = img[int(height / 2): height, 0:width]
    return cropped_img


def get_angle(img):
    # Convert the image to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply Canny edge detection to obtain a binary image
    edges = cv2.Canny(gray, 100, 200)

    # Apply morphological dilation to fill in gaps in the edges
    kernel = np.ones((3, 3), np.uint8)
    dilated = cv2.dilate(edges, kernel, iterations=1)

    # Apply Hough line transform to detect lines that fit the edges
    lines = cv2.HoughLines(dilated, 1, np.pi / 180, 100)

    # Filter the detected lines by their angle
    horizontal_lines = []

    if lines is None:
        return 0

    for line in lines:
        rho, theta = line[0]
        angle = theta * 180 / np.pi
        if abs(angle - 90) < 10:
            horizontal_lines.append(line)

    # Calculate the angle of the line that best fits the edges of the text
    if len(horizontal_lines) > 0:
        rho, theta = horizontal_lines[0][0]
        angle = theta * 180 / np.pi - 90
        # print(f"The license plate is rotated by {angle} degrees.")
        return angle
    else:
        # print("No horizontal lines detected.")
        return 0


def rotate_image(image, angle):
    # Determine image center
    height, width = image.shape[:2]
    center = (width // 2, height // 2)

    # Calculate the rotation matrix
    rotation_matrix = cv2.getRotationMatrix2D(center, angle, 1.0)

    # Determine the new bounding dimensions of the rotated image
    cos_theta = np.abs(rotation_matrix[0, 0])
    sin_theta = np.abs(rotation_matrix[0, 1])
    new_width = int((height * sin_theta) + (width * cos_theta))
    new_height = int((height * cos_theta) + (width * sin_theta))

    # Adjust the rotation matrix to take into account translation
    rotation_matrix[0, 2] += (new_width / 2) - center[0]
    rotation_matrix[1, 2] += (new_height / 2) - center[1]

    # Perform the actual rotation with white background padding
    rotated_image = cv2.warpAffine(image, rotation_matrix, (new_width, new_height), borderValue=(255, 255, 255))

    return rotated_image


def filter_special_character(input_string, language, class_label=None, class_prob=None):
    char_dict = None
    if language == "vn_cam":
        char_dict = vn_cam_char_dict
    elif language == "vn_ch":
        char_dict = vn_ch_char_dict
    elif language == "en":
        char_dict = vn_ch_char_dict
    elif language == "vn_lao":
        char_dict = vn_lao_char_dict

    final_string = []
    for char in input_string:
        if char_dict is None:
            raise Exception("The country is not correct")
        if char in char_dict:
            # uppercase for letter
            char = char.upper()
            final_string.append(char)
    return final_string


def filter_number_by_ratio(numbers):
    global LAO_RATIO_REGION
    max_value = max(numbers)

    filtered_idx = []

    for idx, num in enumerate(numbers):
        ratio = max_value / num
        print(ratio)
        if ratio < LAO_RATIO_REGION:
            filtered_idx.append(idx)
    return filtered_idx


def filter_block_by_area(regions):
    """ Return the index of max area among the areas. Only apply for Lao country """
    areas = []
    height_list = []
    for _, region in enumerate(regions):
        tl = region[0]
        br = region[2]
        tr = region[1]
        lp_area = (br[0] - tl[0]) * (br[1] - tl[1])
        logger.critical(f"height {(br[1] - tr[1])}")
        logger.critical(f"width {(br[0] - tl[0])}")
        height_list.append(br[1] - tr[1])
        areas.append(lp_area)

    filtered_indices = filter_number_by_ratio(height_list)

    if len(regions) != 1:
        return filtered_indices
    else:
        # case only one item in list
        return [0]


def parse_result(results, threshold, image_pil, language, debug=False, det=True, cv_image=None, file_path="",
                 class_label="", class_prob=None):
    result = results[0]
    regions = [line[0] for line in result]
    logger.info(result)

    if len(regions) != 0 and det is True:
        most_suitable_text_block = filter_block_by_area(regions)
        # update the result
        print(regions, most_suitable_text_block)
        result = [result[i] for i in most_suitable_text_block]

    texts, scores = [], []
    if not det:
        for item in result:
            text, score = item[0], item[1]
            if score > threshold:
                texts.append(text)
                scores.append(score)
    else:
        for item in result:
            text, score = item[1][0], item[1][1]
            if score > threshold:
                texts.append(text)
                scores.append(score)

    recognized_text = ""
    for text in texts:
        recognized_text += text

    logger.info(f"Before Filtering: {recognized_text}")
    recognized_text = filter_special_character(recognized_text, language, class_label, class_prob)
    recognized_text = "".join(recognized_text)
    logger.info(f"After Filtering: {recognized_text}")

    dict_data = {
        "regions": regions,
        "texts": texts,
        "scores": scores
    }

    corrected_text = correct_character(recognized_text)
    corrected_text = replace_first_char(corrected_text)
    logger.info(f"After Correction: {corrected_text}")

    # rule base for each country
    corrected_text = apply_rule_for_country(corrected_text, language, class_label, class_prob)
    logger.info(f"After Correction By Language: {corrected_text}")

    if debug:
        if not os.path.exists('src/output'):
            os.makedirs('src/output')

        if det:
            boxes = [line[0] for line in results[0]]
            txts = [line[1][0] for line in results[0]]
            scores = [line[1][1] for line in results[0]]

            font_path = get_font_path(language)

            im_show = draw_ocr(image_pil, boxes, txts, scores, drop_score=0.5, font_path=font_path)

            if len(file_path):
                cv2.imwrite(f"src/output/{os.path.basename(file_path).split('.')[0]}.jpg", im_show)
            else:
                filename = str(uuid.uuid4())[:8]
                cv2.imwrite(f"src/output/{filename}_{corrected_text}.jpg", im_show)
        else:
            # Default Font
            white_img = np.zeros((cv_image.shape[0], cv_image.shape[1], 3), dtype=np.uint8)
            font_path = get_font_path(language)

            # Load TTF font file
            white_img = Image.fromarray(white_img)
            draw = ImageDraw.Draw(white_img)
            # Define text and font parameters
            font = ImageFont.truetype(font_path, size=20)
            text_color = (255, 255, 255)

            # Get text size to center it properly
            text_x, text_y = 5, 5

            draw.text((text_x, text_y), recognized_text, font=font, fill=text_color)

            opencv_image = np.array(white_img)
            output_img = cv2.hconcat([cv_image, opencv_image])

            if len(file_path):
                cv2.imwrite(f"src/output/{os.path.basename(file_path).split('.')[0]}.jpg", output_img)
            else:
                filename = str(uuid.uuid4())[:8]
                cv2.imwrite(f"src/output/{filename}_{corrected_text}.jpg", output_img)

    return corrected_text, dict_data


def get_font_path(language):
    # Default Font
    font_path = "src/fonts/NotoSans-Regular.ttf"
    if language == "vn_cam":
        font_path = "src/fonts/chinese.msyh.ttf"
    elif language == "vn_lao":
        font_path = "src/fonts/lao_saysettha_ot.ttf"
    elif language == "vn_ch":
        font_path = "src/fonts/chinese.msyh.ttf"
    return font_path


def save_image(image_array, save_path):
    # Create a PIL Image from the image array
    image_pil = Image.fromarray(image_array)

    # Save the image to the specified path
    image_pil.save(save_path)


def draw_bounding_boxes(image_array, regions, color=(0, 255, 0), thickness=1):
    # Create a PIL Image from the image array
    image_pil = Image.fromarray(np.uint8(image_array))

    # Create a PIL ImageDraw object
    draw = ImageDraw.Draw(image_pil)

    # Iterate over each region
    for region in regions:
        tl, tr, br, bl = region
        x_min, y_min = tl
        x_max, y_max = br
        x_min, y_min, x_max, y_max = int(x_min), int(y_min), int(x_max), int(y_max)

        draw.rectangle((x_min, y_min, x_max, y_max), outline=color, width=thickness)

    # Convert the PIL Image back to a NumPy array
    image_with_boxes = np.array(image_pil)

    return image_with_boxes


def parse_base64_to_numpy(base64_string):
    # Remove the data URL prefix if present
    if base64_string.startswith('data:image'):
        base64_string = base64_string.split(',')[1]

    # Decode the Base64 string
    image_data = base64.b64decode(base64_string)

    # Create a BytesIO object
    image_bytes = io.BytesIO(image_data)

    # Open the image using PIL
    pil_image = Image.open(image_bytes)

    if pil_image.mode != "RGB":
        pil_image = pil_image.convert("RGB")

    # Convert PIL image to NumPy array
    numpy_array = np.array(pil_image)

    return numpy_array, pil_image


def check_vietnamese_license_plate(license_plate):
    # Rule 1: The license plate should have a length of 9 characters.
    if len(license_plate) < 4:
        return False

    if len(license_plate) > 11:
        return False

    return True


def is_chinese_char(char):
    """Check if a character is a Chinese character."""
    return 'CJK' in unicodedata.name(char)


def correct_character(license_plate):
    # If the first character is Chinese character and len of license plate == 7
    # rule base for cambodia
    if len(license_plate) == 7 and is_chinese_char(license_plate[0]):
        return license_plate

    if not len(license_plate) >= 7:
        return license_plate
    else:
        # correction for chinese
        corrections = {
            '1': 'I',
            '2': 'Z',
            '3': 'B',
            '4': 'A',
            '5': 'S',
            '6': 'C',
            '7': 'T',
            '8': 'B',
        }

        if corrections.get(license_plate[2]) is None:
            return license_plate

        corrected_license_plate = license_plate[:2] + corrections[license_plate[2]] + license_plate[3:]
        return corrected_license_plate


def replace_first_char(string):
    """ Fix the error for S"""
    if len(string) >= 6 and string[0] == 'S':
        string = '5' + string[1:]
    return string


def apply_rule_for_country(text, language, class_label, class_prob):
    if language == "vn_cam":
        text = text.replace("GBA25165", "68A25165").replace("200200", "2Q0200").replace("241250", "2A1250").replace(
            "211250", "2A1250").replace("271250", "2A1250").replace("6BA", '68A').replace("0B3590", "QB3590").replace(
            "2A09296", "2AQ9296").replace("2AO9296", "2AQ9296")

        # text = AUICN2BJ6309=> get last 7 character
        if len(text) > 9:
            text = text[-7::]

        return text

    if language == "vn_lao" and class_label == "Lao" and class_prob > 0.90:
        if len(text) > 6:
            text = text[-6:]
        return text

    return text


@app.post("/parse")
async def parse(request: MyRequest):
    global country_dict

    t1 = time.time()
    logger.info("[*] Start inference")
    base64_string = request.base64
    threshold = request.threshold
    language = request.language

    np_image, image_pil = parse_base64_to_numpy(base64_string)

    # Get ocr model
    ocr_model = get_ocr_model(language)

    # 1. Run LP Classification
    class_label, class_prob = lp_cls_model.detect(image_pil, language)

    logger.info(f"Class Label: {class_label} | Probs: {class_prob}")

    angle = get_angle(np_image)
    logger.info(f"[*] Angle: {angle}")
    rotated_image = rotate_image(np_image, angle)

    recognized_text, dict_data = predict(rotated_image, ocr_model, language, class_label, class_prob, threshold)

    logger.debug("[*] Inference Time: ", time.time() - t1)
    logger.info(f"Recognized Text: {recognized_text} | {country_dict[class_label]} | {time.time() - t1}s")
    return {"text": recognized_text, "country": country_dict[class_label], "status": "Request parsed successfully",
            "detail": dict_data}


def get_ocr_model(language: str):
    ocr_models = {
        "vn_cam": vn_cam,
        "vn_ch": vn_ch,
        "vn_lao": vn_cam
    }

    if language not in ocr_models:
        raise Exception("Language is not correct")

    return ocr_models[language]


def run_lp_classification(image, language: str):
    class_label, class_prob = lp_cls_model.detect(image, language)
    logger.info(f"Class Label: {class_label}")
    return class_label, class_prob


def get_original_det_boxes(detection_boxes, original_image_size, resized_image_size):
    # Unpack the original and resized image sizes
    original_height, original_width = original_image_size
    resized_height, resized_width = resized_image_size

    # Calculate the scaling factors in x and y directions
    scale_x = original_width / resized_width
    scale_y = original_height / resized_height

    for i in range(len(detection_boxes)):
        # Scale the detection bounding boxes back to the original coordinates
        detection_boxes[i][:, 0] *= scale_x
        detection_boxes[i][:, 1] *= scale_y

    return detection_boxes


def predict(image, ocr_model, language: str, class_label: str, class_prob: float, threshold: float):
    dict_data = {}

    # use the ocr_model if class_label is Lao
    if class_label == "Lao" and language == "vn_lao" and class_prob >= 0.80:
        # 1.Turn on detection
        # 2. Identify how many blocks
        # 3. Select the largest blocks
        original_image = copy.deepcopy(image)
        original_image_size = original_image.shape[:2]
        image = cv2.resize(image, (640, 640))
        resized_image_size = (640, 640)

        det_flag = True

        ocr_res = []

        start_time = time.time()
        # use vn_lao text detector
        dt_boxes, _ = vn_lao.text_detector(image)

        recognized_text = []

        if dt_boxes is not None or len(dt_boxes) == 0:
            dt_boxes = sorted_boxes(dt_boxes)
            original_boxes = get_original_det_boxes(dt_boxes, original_image_size, resized_image_size)

            img_crop_list = []
            for bno in range(len(original_boxes)):
                tmp_box = copy.deepcopy(original_boxes[bno])
                img_crop = get_rotate_crop_image(original_image, tmp_box)
                img_crop_list.append(img_crop)

            # use vn_lao text recognizer
            rec_res, _ = vn_lao.text_recognizer(img_crop_list)
            filter_boxes, filter_rec_res = [], []
            for box, rec_result in zip(original_boxes, rec_res):
                text, score = rec_result
                if score > threshold:
                    filter_boxes.append(box)
                    filter_rec_res.append(rec_result)

            tmp_res = [[box.tolist(), res] for box, res in zip(filter_boxes, filter_rec_res)]
            ocr_res.append(tmp_res)

            recognized_text, dict_data = parse_result(ocr_res, 0.6, original_image, language=language, debug=DEBUG,
                                                      det=det_flag, cv_image=original_image, class_label=class_label,
                                                      class_prob=class_prob)
        # 4. Re-run if recognized text is empty
        if (len(recognized_text)) == 0 or (dt_boxes is None) or (len(dt_boxes) == 0):
            logger.critical("Not found the text detection.")
            det_flag = False
            cropped_img = crop_bottom_half(image)
            # because the image is already cropped than no need detection
            results = vn_lao.ocr(cropped_img, det=det_flag, rec=True, cls=False)
            recognized_text, dict_data = parse_result(results, 0.6, cropped_img, language=language, debug=DEBUG,
                                                      det=det_flag, cv_image=image, class_label=class_label,
                                                      class_prob=class_prob)
    # another country
    else:
        logger.critical("Other country")
        results = ocr_model.ocr(image, det=True, rec=True, cls=False)
        recognized_text, dict_data = parse_result(results, threshold, image, language, debug=DEBUG,
                                                  class_label=class_label, class_prob=class_prob)

        # 2. Re-run if recognized text is empty
        if len(recognized_text) == 0:
            results = ocr_model.ocr(image, det=False, rec=True, cls=False)
            # 2.1 Run with normal threshold_conf
            recognized_text, dict_data = parse_result(results, THRESHOLD_CONF, image, language, debug=DEBUG,
                                                      det=False, cv_image=image)
            # 2.2 Run with very low threshold_conf
            if len(recognized_text) == 0:
                recognized_text, dict_data = parse_result(results, 0.2, image, language, debug=DEBUG, det=False,
                                                          cv_image=image)

    return recognized_text, dict_data


@app.post("/upload")
async def upload_image(file: Optional[UploadFile] = File(default=None),
                       language: str = Query("vn_cam", enum=["vn_cam", "vn_lao", "vn_ch"]),
                       folder_dir: str = ""):
    """ Predict single image and many images """
    global country_dict

    threshold = THRESHOLD_CONF

    # Get ocr model
    ocr_model = get_ocr_model(language)

    # single image
    if len(folder_dir) == 0:

        # Save the uploaded image to a desired location
        pil_image = Image.open(file.file)

        if pil_image.mode != "RGB":
            pil_image = pil_image.convert("RGB")

        # Convert PIL image to NumPy array
        numpy_array = np.array(pil_image)

        # run LP Classification
        class_label, class_prob = lp_cls_model.detect(pil_image, language)

        angle = get_angle(numpy_array)
        rotated_image = rotate_image(numpy_array, angle)
        print(f"[*] Angle: {angle}")

        recognized_text, dict_data = predict(rotated_image, ocr_model, language, class_label, class_prob, threshold)

        return {"text": recognized_text, "country": country_dict[class_label], "status": "Request parsed successfully",
                "detail": dict_data}

    # multiple images
    else:
        image_extensions = ['.jpg', '.jpeg', '.png']
        files = []
        for filename in os.listdir(folder_dir):
            extension = os.path.splitext(filename)[-1].lower()
            if extension in image_extensions:
                files.append(os.path.join(folder_dir, filename))

        print("Number of files: ", len(files))

        recognized_texts = []

        output_label = open("src/output/labels.txt", "w")

        for file in files:
            print(f"[*] File: {file}")
            cv_image = cv2.imread(os.path.join(folder_dir, file))
            if ".jpg" or ".png" in os.path.join(folder_dir, file):
                rgb_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)

                # 1. Run LP Classification
                class_label, class_prob = lp_cls_model.detect(rgb_image, language)

                print(f"Class Label: {class_label} | File: {file} | Probs: {class_prob}")

                angle = get_angle(rgb_image)
                print(f"[*] Angle: {angle}")
                rotated_image = rotate_image(rgb_image, angle)
                recognized_text, dict_data = predict(rotated_image, ocr_model, language, class_label, class_prob,
                                                     threshold)

                recognized_texts.append(recognized_text)
                output_label.write(f"{os.path.basename(file)}\t{recognized_text}\n")

        output_label.close()

        return {"status": "done", "text": recognized_texts}


@app.post("/convert_base64")
async def convert_base64(file: UploadFile = File(...)):
    pil_image = Image.open(file.file)

    buffered = io.BytesIO()
    pil_image.save(buffered, format="JPEG")
    image_bytes = buffered.getvalue()

    # Encode the bytes as base64 and convert them to a string
    base64_encoded_image = base64.b64encode(image_bytes).decode('utf-8')

    # Print the resulting base64-encoded image string
    return base64_encoded_image


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8500, reload=False)
    # uvicorn.run("main:app", host="0.0.0.0", port=8500, reload=True)

    # image_path = "/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/1685202835536_image.jpg"
    # image_pil = Image.open(image_path)
    # results = en_ocr.ocr(image_path, det=True, rec = True, cls=False)
    # recognized_text, dict_data = parse_result(results,0.6,image_pil)
    # print(recognized_text)
