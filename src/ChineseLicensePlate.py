import re
from LicensePlateRule import LicensePlateRule

class ChineseLicensePlate(LicensePlateRule):
    def __init__(self):
        super().__init__('CN')

    def is_valid(self, license_plate):
        # Chinese license plates have the format "A-NNNNN" where A is an uppercase letter and N is a digit
        if not re.match(r'^[A-Z]-\d{5}$', license_plate):
            return False
        
        # The first letter of the license plate cannot be I,O, or V
        forbidden_letters = ['I', 'O', 'V']
        if license_plate[0] in forbidden_letters:
            return False
        
        return True