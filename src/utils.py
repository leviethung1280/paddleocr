import base64
import datetime
import io
import os

import openpyxl
from PIL import Image as PILImage
from openpyxl import Workbook
from openpyxl.drawing.image import Image


def insert_images_auto_scale(image_paths, predictions, excel_file_path, start_row, max_width, max_height,
                             cell_image_width=25):
    wb = Workbook()
    sheet = wb.active

    # Set column headers
    sheet.cell(row=1, column=1, value='Image')
    sheet.cell(row=1, column=2, value='File Name')
    sheet.cell(row=1, column=3, value='Created Date')
    sheet.cell(row=1, column=4, value='Prediction')
    sheet.cell(row=1, column=5, value='Correct?')
    
    # Set column widths
    sheet.column_dimensions['A'].width = cell_image_width
    sheet.column_dimensions['B'].width = 25
    sheet.column_dimensions['C'].width = 20
    sheet.column_dimensions['D'].width = 15
    sheet.column_dimensions['E'].width = 15
    
    # Center align the header cells
    for col in range(1, sheet.max_column + 1):
        header_cell = sheet.cell(row=1, column=col)
        header_cell.alignment = openpyxl.styles.Alignment(horizontal='center', vertical='justify')

    evaluations = [""] * len(predictions)

    # Insert data and images
    for i, (image_path, prediction, evaluation) in enumerate(zip(image_paths, predictions, evaluations),
                                                             start=start_row):
        img = PILImage.open(image_path)
        img_width, img_height = img.size
        basename = os.path.basename(image_path)

        # Calculate scaling factors to fit the image within the max width and height
        width_scale = max_width / img_width
        height_scale = max_height / img_height
        scale = min(width_scale, height_scale)

        # Resize the image
        img = img.resize((int(img_width * scale), int(img_height * scale)), PILImage.LANCZOS)

        # Convert PIL image to bytes
        img_io = io.BytesIO()
        img.save(img_io, format='PNG')

        # Convert bytes to openpyxl image
        openpyxl_img = Image(img_io)

        # Insert image into the cell
        img_cell = sheet.cell(row=i, column=1)
        sheet.add_image(openpyxl_img, img_cell.coordinate)

        # Insert image path with alignment
        path_cell = sheet.cell(row=i, column=2)
        path_cell.value = basename
        path_cell.alignment = openpyxl.styles.Alignment(horizontal='left', vertical='justify', wrap_text=True)

        # Insert create_date, prediction, and evaluation data with alignment
        create_date_cell = sheet.cell(row=i, column=3)
        create_date_cell.value = datetime.datetime.now().strftime('%H:%M:%S %d-%m-%Y')
        create_date_cell.alignment = openpyxl.styles.Alignment(horizontal='center', vertical='justify')

        # Insert prediction and evaluation data with alignment
        prediction_cell = sheet.cell(row=i, column=4)
        prediction_cell.value = prediction
        prediction_cell.alignment = openpyxl.styles.Alignment(horizontal='center', vertical='justify')

        evaluation_cell = sheet.cell(row=i, column=5)
        evaluation_cell.value = evaluation
        evaluation_cell.alignment = openpyxl.styles.Alignment(horizontal='center', vertical='justify')

        # Calculate required row height based on scaled image height
        required_row_height = img_height * scale

        # Adjust row height to fit the image
        sheet.row_dimensions[i].height = required_row_height

    # Save the workbook
    wb.save(excel_file_path)


def get_images(test_dir):
    image_extensions = ['.jpg', '.jpeg', '.png']

    image_paths = []
    for filename in os.listdir(test_dir):
        extension = os.path.splitext(filename)[-1].lower()
        if extension in image_extensions:
            image_paths.append(os.path.join(test_dir, filename))

    return image_paths


def open_base64(image_path):
    # convert image to base64
    with open(image_path, "rb") as f:
        image_data = f.read()
        encoded_data = base64.b64encode(image_data).decode("utf-8")
    return encoded_data


if __name__ == "__main__":
    # Example usage
    image_paths = ['/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test_images/50E06344.jpg',
                   '/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test_images/50E06344.jpg',
                   '/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test_images/50E06344.jpg']
    predictions = ['ABC123', 'DEF456', 'GHI789']
    evaluations = ['Correct', 'Incorrect', 'Correct']
    insert_images_auto_scale(image_paths, predictions, evaluations, 'output.xlsx', start_row=2, max_width=130,
                             max_height=130)
