import cv2
import numpy as np


def get_rotate_angle(image_path):
    # Load the license plate image
    img = cv2.imread(image_path)
    # Convert the image to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply Canny edge detection to obtain a binary image
    edges = cv2.Canny(gray, 100, 200)

    # Apply morphological dilation to fill in gaps in the edges
    kernel = np.ones((3, 3), np.uint8)
    dilated = cv2.dilate(edges, kernel, iterations=1)

    # Apply Hough line transform to detect lines that fit the edges
    lines = cv2.HoughLines(dilated, 1, np.pi/180, 100)

    # Filter the detected lines by their angle
    horizontal_lines = []
    for line in lines:
        rho, theta = line[0]
        angle = theta * 180 / np.pi
        if abs(angle - 90) < 10:
            horizontal_lines.append(line)

    print("Horizontal Lines:", horizontal_lines)

    # Calculate the angle of the line that best fits the edges of the text
    if len(horizontal_lines) > 0:
        rho, theta = horizontal_lines[0][0]
        angle = theta * 180 / np.pi - 90
        print(f"The license plate is rotated by {angle} degrees.")
    else:
        print("No horizontal lines detected.")



cv2.imshow("Original License Plate", gray)
cv2.imshow("Straightened License Plate", rotated_image)
cv2.imshow("Straightened License Plate - Edge", edges)
cv2.imshow("Straightened License Plate - Dilated", dilated)
cv2.waitKey(0)
cv2.destroyAllWindows()
