import unicodedata

def is_chinese_char(char):
    """Check if a character is a Chinese character."""
    return 'CJK' in unicodedata.name(char)

# Example usage
char = '你'
char = 'N'
if is_chinese_char(char):
    print(f'{char} is a Chinese character.')
else:
    print(f'{char} is not a Chinese character.')
