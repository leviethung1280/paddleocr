import cv2
import numpy as np

# Load the image
image = cv2.imread("/home/hunglv/Downloads/download.jpeg")

# Convert the image to grayscale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Apply Gaussian blur to reduce noise
blurred = cv2.GaussianBlur(gray, (5, 5), 0)

# Apply Canny edge detection
edges = cv2.Canny(blurred, 50, 150)

# Find contours in the image
contours, _ = cv2.findContours(edges.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Sort contours based on their area (in descending order)
contours = sorted(contours, key=cv2.contourArea, reverse=True)[:10]

print (contours)

# Iterate over the contours and find the license plate contour
license_plate_contour = None
for contour in contours:
    # Approximate the contour to a polygon
    perimeter = cv2.arcLength(contour, True)
    approx = cv2.approxPolyDP(contour, 0.02 * perimeter, True)

    # Check if the polygon has 4 sides
    if len(approx) == 4:
        license_plate_contour = approx
        break

if license_plate_contour is not None:
    # Draw the license plate contour on the original image
    cv2.drawContours(image, [license_plate_contour], -1, (0, 255, 0), 2)

    # Display the image with the detected license plate contour
    cv2.imshow('License Plate', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
else:
    print("Not thing")
