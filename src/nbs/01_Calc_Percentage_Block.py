# import cv2
# from paddleocr import PaddleOCR

# image_path = "/media/hunglv/350GB_1TB_HDD/02_Lao_chiengkhuong_lpr/train/ChienKhuong_LPR_23022023/images/1eae4b67-37e3-4aca-a640-ecefe8cb8026.jpg"

# vn_lao = PaddleOCR(lang='en', det_model_dir="/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/ocr_sharp/ch_v3/ch_PP-OCRv3_det_infer", det_db_box_thresh=0.618, det_db_score_mode='slow', det_db_unclip_ratio=1.6, rec_model_dir="/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/models/ocr/lao_vn/lao_PP-OCRv3-v1.0_rec", rec_char_dict_path="/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/src/models/ocr/lao_vn/lao_PP-OCRv3-v1.0_rec/vi_lao_dict.txt")
# results = vn_lao.ocr(image_path, det=True, rec = True, cls=False)

# result = results[0]
# regions = [line[0] for line in result]

# image = cv2.imread(image_path)
# h,w = image.shape[:2]


# tl = (16,8)
# br = (76,35)


# img_area = h * w 
# lp_area = (br[0] - tl[0]) * (br[1] - tl[1])

# print(lp_area/img_area)




regions = [[[50.0, 23.0], [155.0, 26.0], [154.0, 59.0], [50.0, 56.0]], [[9.0, 75.0], [194.0, 75.0], [194.0, 164.0], [9.0, 164.0]]]
areas = []
print(regions)
for region in regions:
    print('regions:',region)
    tl = region[0]
    br = region[2]
    print(tl,br)
    lp_area = abs(br[0] - tl[0]) * abs(br[1] - tl[1])
    print(lp_area)
    areas.append(lp_area)
print(areas)