import cv2
import numpy as np

def straighten_license_plate(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    edges = cv2.Canny(blurred, 50, 150)

    lines = cv2.HoughLinesP(edges, 1, np.pi / 180, threshold=100, minLineLength=100, maxLineGap=10)
    
    print(lines)

    # Filter and select dominant lines
    selected_lines = []
    for line in lines:
        x1, y1, x2, y2 = line[0]
        angle = np.arctan2(y2 - y1, x2 - x1) * 180 / np.pi
        # Filter lines based on orientation or position
        
        print(angle)

        selected_lines.append(line)

    # Calculate rotation angle
    rotation_angle = np.mean([np.arctan2(y2 - y1, x2 - x1) for line in selected_lines])

    # Apply rotation transformation
    rotated_image = cv2.warpAffine(image, cv2.getRotationMatrix2D((image.shape[1] // 2, image.shape[0] // 2), rotation_angle, 1), (image.shape[1], image.shape[0]))

    return rotated_image

# Example usage

image_path = "/media/hunglv/350GB_1TB_HDD/2022-01-20-2_vn/3bbb7ce0-4459-467b-b51c-f8c614b50407.jpg"
image_path = "/media/hunglv/350GB_1TB_HDD/2022-01-20-2_vn/3dcc54bb-1246-43d2-a93d-ffe1270158e5.jpg"
image_path = "/media/hunglv/350GB_1TB_HDD/2022-01-20-2_vn/9fbe88f4-9eaa-471b-99d8-f717aa455bcf.jpg"
image_path = "/media/hunglv/350GB_1TB_HDD/2022-01-20-2_vn/655a7def-9e8a-46b8-b330-131967fe2564.jpg"




image = cv2.imread(image_path)  # Replace "license_plate.jpg" with your license plate image file path
straightened_image = straighten_license_plate(image)

# Display the original license plate image and the straightened image
cv2.imshow("Original License Plate", image)
cv2.imshow("Straightened License Plate", straightened_image)
cv2.waitKey(0)
cv2.destroyAllWindows()

