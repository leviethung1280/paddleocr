# denoising


# sharpening

# constrast adjust ment

# historgram euqlaice 





# brightness of iamge
# import cv2
import numpy as np

# # Load the image
# image = cv2.imread('/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_hard/0c4d3028-6745-479a-bffe-1e4b0345fb2c.jpg')

# # Increase brightness by adding a constant value
# brightness_offset = 75  # Adjust the value to control the brightness level
# brighter_image = np.clip(image.astype(int) + brightness_offset, 0, 255).astype(np.uint8)

# # Display the original and brighter image side by side
# combined_image = np.hstack((image, brighter_image))
# cv2.imshow('Original vs. Brighter Image', combined_image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# thresholding 
import cv2

# # Load the image
# image = cv2.imread('/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_hard/0c4d3028-6745-479a-bffe-1e4b0345fb2c.jpg', 0)  # Read the image as grayscale

# # Apply thresholding
# _, thresholded = cv2.threshold(image, 50, 255, cv2.THRESH_BINARY)

# # Display the thresholded image
# cv2.imshow('Thresholded Image', thresholded)
# cv2.waitKey(0)
# cv2.destroyAllWindows()


# def histogram_equalization(image):
#     # Convert the image to grayscale if it's in color
#     if len(image.shape) > 2:
#         gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#     else:
#         gray = image
    
#     # Apply histogram equalization
#     equalized = cv2.equalizeHist(gray)
    
#     return equalized

# # Load the image
# image = cv2.imread('/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_hard/0c4d3028-6745-479a-bffe-1e4b0345fb2c.jpg')

# # Apply histogram equalization
# equalized_image = histogram_equalization(image)


# # Display the original and equalized images side by side
# # combined_image = np.hstack((image, equalized_image))
# # cv2.imshow('Original vs. Equalized Image', combined_image)
# # cv2.waitKey(0)
# # cv2.destroyAllWindows()

# cv2.imshow('Original vs. Equalized Image', equalized_image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()


import cv2

# Load the image
# image = cv2.imread('/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_hard/0c4d3028-6745-479a-bffe-1e4b0345fb2c.jpg')
# image = cv2.imread('/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_hard/0ee7a2aa-fc3a-4878-a8ed-7ec4ef3b8379.jpg')
# image = cv2.imread('/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_hard/0d38750e-4dfc-48ad-b054-aa8f104e6bbf.jpg')
image = cv2.imread('/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_hard/1bab06a8-9f23-4448-872d-affd3545d655.jpg')


# Convert the image to grayscale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Apply adaptive thresholding to obtain a binary image
_, binary = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)

# apply connected component analysis to the thresholded image
output = cv2.connectedComponentsWithStats(
	binary,4, cv2.CV_32S)
(numLabels, labels, stats, centroids) = output

mask = np.zeros(gray.shape, dtype="uint8")


print(numLabels, labels,stats,centroids)
print(numLabels)
for i in range(1, numLabels):
    # extract the connected component statistics for the current
    # label
    x = stats[i, cv2.CC_STAT_LEFT]
    y = stats[i, cv2.CC_STAT_TOP]
    w = stats[i, cv2.CC_STAT_WIDTH]
    h = stats[i, cv2.CC_STAT_HEIGHT]
    area = stats[i, cv2.CC_STAT_AREA]
    
    # keepWidth = w > 5 and w < 100
    # keepHeight = h > 40 and h < 75
        
    keepWidth = w > 5 
    keepHeight = h > 40 
    # ensure the connected component we are examining passes all
    # three tests
    if all((keepWidth, keepHeight)):
        # construct a mask for the current connected component and
        # then take the bitwise OR with the mask
        print("[INFO] keeping connected component '{}'".format(i))
        componentMask = (labels == i).astype("uint8") * 255
        mask = cv2.bitwise_or(mask, componentMask)


# Find contours in the binary image
contours, _ = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Filter contours based on area or other criteria if needed
min_area_threshold = 20
filtered_contours = [cnt for cnt in contours if cv2.contourArea(cnt) > min_area_threshold]

# Create a copy of the original image to draw bounding boxes
bounding_box_image = image.copy()

# Draw bounding boxes around the filtered contours
print("Number of contours:", len(filtered_contours))


lp_contours = []
for contour in filtered_contours:
    x, y, w, h = cv2.boundingRect(contour)
    aread = cv2.contourArea(contour)
    aspect_ratio = w / float(h)
    # set the desired criteria to filter out noise bounding boxes
    if aspect_ratio > 2:
        continue
    cv2.rectangle(bounding_box_image, (x, y), (x + w, y + h), (0, 255, 0), 2)
    lp_contours.append(contour)
    
x_coordinates, y_coordinates = [], []
for contour in lp_contours:
    x,y, w, h= cv2.boundingRect(contour)
    x_coordinates.append(x)
    y_coordinates.append(y)
    x_coordinates.append(x+w)
    y_coordinates.append(y+h)
    
# print(lp_contours)
# Get the minimum and maximum x and y coordinates
min_x = min(x_coordinates)
max_x = max(x_coordinates)
min_y = min(y_coordinates)
max_y = max(y_coordinates)

line_image = image.copy()
cv2.rectangle(line_image, (min_x, min_y), (max_x, max_y), (0, 255, 0), 2)

# Create a copy of the original image to draw contours
contour_image = image.copy()

# Draw contours on the contour image
cv2.drawContours(contour_image, filtered_contours, -1, (0, 255, 0), 2)

# Display the contour image
cv2.imshow('Contour Image', contour_image)
cv2.imshow('Bounding Box Image', bounding_box_image)
cv2.imshow('Line Image', line_image)
cv2.imshow('maks Image', mask)
cv2.waitKey(0)
cv2.destroyAllWindows()