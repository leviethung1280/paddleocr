from PIL import Image

def crop_bottom_half(image_path):
    with Image.open(image_path) as im:
        width, height = im.size
        cropped_im = im.crop((0,height/2, width, height))
        cropped_im.show()            

crop_bottom_half('/mnt/01D624E6112E18C0/tlt-experiments/JP_OCR/paddleocr/test/lao_hard/1bab06a8-9f23-4448-872d-affd3545d655.jpg')