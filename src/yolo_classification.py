import numpy as np 
from ultralytics import YOLO
from PIL import Image

class YOLOClassification:
    def __init__(self, model_path) -> None:
        self.model = YOLO(model_path)
        
    def initialize(self):
        """ Inference for dummy image """
        self.model.predict(Image.open("src/dummy_img.jpg"))

    def detect(self, image_pil, language):
        """  Run the license plate classification
        names_dict: {0: 'Cambodia', 1: 'China', 2: 'Lao', 3: 'Vietnam_1_Line', 4: 'Vietnam_2_Line'} 
        probs: [   0.085263   0.0012602  0.00072166   0.0025801     0.91018]
        """
        cls_result = self.model.predict(image_pil)
        names_dict = cls_result[0].names
        probs = cls_result[0].probs.data.cpu().numpy()

        class_label, class_prob = self.filter_by_language(language, names_dict, probs)

        return class_label, class_prob

    def filter_by_language(self, language, name_dict, probs):
        if language == "vn_cam":
            my_list = ["Vietnam_2_Line", "Vietnam_1_Line", "Cambodia"]
        elif language == "vn_ch":
            my_list =  ["Vietnam_2_Line", "Vietnam_1_Line", "China"]
        elif language == "vn_lao":
            my_list =  ["Vietnam_2_Line", "Vietnam_1_Line", "Lao"]
        else:
            print("Language input is not supported !!!")
            my_list = ["Vietnam_2_Line", "Vietnam_1_Line", "Lao", "Cambodia", "China"]
            
        sorted_countries = [(x,prob) for prob, x in sorted(zip(probs,list(name_dict.values())), reverse=True)]
        
        print(sorted_countries)
        
        filtered_countries = [k[0] for k in sorted_countries if k[0] in my_list]
        filtered_probs = [k[1] for k in sorted_countries if k[0] in my_list]
        
        return filtered_countries[0], filtered_probs[0]                            


if __name__ == "__main__":
    image_pil = Image.open("test_images/60H00948.jpg")
    lp_cls_model = YOLOClassification("./src/LP_Classification/06122023_4_countries_v3.pt")
    clss_name, label = lp_cls_model.detect(image_pil, "vn_ch")
    print(f"{clss_name} - {label}")
    clss_name, label =  lp_cls_model.detect(image_pil, "vn_cam")
    print(f"{clss_name} - {label}")
    clss_name, label =  lp_cls_model.detect(image_pil, "vn_lao")
    print(f"{clss_name} - {label}")
    clss_name, label =  lp_cls_model.detect(image_pil, "vn")
    print(f"{clss_name} - {label}")
