import re
from LicensePlateRule import LicensePlateRule

class VietnamLicensePlate(LicensePlateRule):
    def __init__(self):
        super().__init__('VN')

    def is_valid(self, license_plate):
        # Vietnamese license plates have the format "X-NNN-NN" where X is a letter and N is a digit
        if not re.match(r'^[A-Z]-\d{3}-\d{2}$', license_plate):
            return False
        
        first_letter = license_plate[0]
        forbidden_letters = ['E', 'U', 'V']
        
        # The first letter of the license plate cannot be E, U or V
        if first_letter in forbidden_letters:
            return False
        
        return True