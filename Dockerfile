FROM python:3.8-slim-buster

RUN apt-get update && apt-get install libgomp1 libgl1-mesa-glx libglib2.0-0 -y

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir -r /app/requirements.txt && rm -rf /root/.cache

COPY . .

EXPOSE 8088

# RUN pip uninstall nvidia_cublas_cu11

CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "8088"]